//
//  ContentView.swift
//  Shared
//
//  Created by Mukti on 2020/12/5.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            MapView()
                .frame(height: 300)
                .edgesIgnoringSafeArea(.top)
            AvatarImage()
                .offset(x: /*@START_MENU_TOKEN@*/0.0/*@END_MENU_TOKEN@*/, y: /*@START_MENU_TOKEN@*/-130.0/*@END_MENU_TOKEN@*/)
                .padding(.bottom, -130.0)
            VStack(alignment: .center) {
                Text("本应用仅用于测试")
                    .font(.title2)
                    .padding(.top, 13.0)
                    .padding(.bottom, 13.0)
                Text("请在相关规定下，合理使用本程序。")
                    .fontWeight(.bold)
                HStack {
                    Text("Copyright © Mukti")
                        .foregroundColor(Color.gray)
                        .padding(.top)
                }
            }
            .padding()
            Spacer()
        }
        Spacer()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ContentView()
        }
            
    }
}
