//
//  MockLocationApp.swift
//  Shared
//
//  Created by Mukti on 2021/6/23.
//

import SwiftUI

@main
struct MockLocationApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
