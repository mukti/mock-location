//
//  AvatarView.swift
//  Mukti
//
//  Created by Mukti on 2020/12/6.
//

import SwiftUI

struct AvatarImage: View {
    var body: some View {
        Image("avatar")
            .scaleEffect(/*@START_MENU_TOKEN@*/0.25/*@END_MENU_TOKEN@*/)
            .frame(width: 150.0, height: 150.0)
            .background(/*@START_MENU_TOKEN@*//*@PLACEHOLDER=View@*/Color.white/*@END_MENU_TOKEN@*/)
            .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
    }
}

struct AvatarImage_Previews: PreviewProvider {
    static var previews: some View {
        AvatarImage()
    }
}
